# ATOM: Data Science Interviews  
**ATOM is meeting on Tuesday, 20th February, 6:30pm, at Galvanize!**  

Join us for a highly participative discussion led by Hanna Landrus ! For ATOM this week we will be discussing the good and bad of the technical interview process, in particular how it relates to data science interviews. Come with your opinions and stories from both sides of the fence, hiring manager and interviewee. We might not be able to fix the hiring processes in the two hours we have but hopefully we will come out with a better perspective of the other side of the fence. If you don’t have any opinions on interviews then here are some articles to give you an idea of some of the challenges.

https://blog.wikimedia.org/2017/02/02/hiring-data-scientist/

http://qethanm.cc/2018/01/23/common-mistakes-in-data-science-hiring-part-1/

About ATOM:
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques and problems in the real world. As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !